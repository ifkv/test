```mermaid
graph TD
    A[fa:fa-user Supplier/Client/Admin] -->|trying to invite user| B{Is the Phone or</br> Email valid?}
    B -->|Yes| D{Does the invitation for </br>this contact already exist and active</br> for this supplier / admin / client?}
    D --> |Yes| F{Has this invitation been</br> resent more times than allowed?}
    F --> |Yes| G[Error: Resend linits]:::error
    F --> |No| H[Resend invitation]:::success

    D--> |No| I{Does the user with this</br>contact exist in the database?}
    I --> |Yes| K{Is this user already</br>registered in your company?}
    K --> |Yes| L[Error: User already registered]:::error
    I --> |No| M[Send invitation]:::success
    K --> |No| N[Send invitation]:::success
    
    B -->|No| E[Error: Email/Phone not valid]:::error
  
  classDef error fill:#f96;
  classDef success fill:#7CB305;
  ```